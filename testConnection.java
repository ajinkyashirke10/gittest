package Test;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class testConnection {
	
	public static void  main(String[] args) throws  ClassNotFoundException, SQLException {													
		//Connection URL Syntax: "jdbc:mysql://ipaddress:portnumber/db_name"		
        String dbUrl = "jdbc:oracle:thin:@135.208.43.43:13000:ABPFBFM1";					

		//Database Username		
		String username = "MCKAPP1";	
        
		//Database Password		
		String password = "MCKAPP1";				
		
		

		//Query to Execute		
		String query = "select * from RM1_RESOURCE where resource_status='AVAILABLE' and resource_type_id='8'";	
        
 	    //Load mysql jdbc driver		
   	    Class.forName("oracle.jdbc.driver.OracleDriver");	
   	    
   	 
   
   		//Create Connection to DB		
    	Connection con = DriverManager.getConnection(dbUrl,username,password);
  
  		//Create Statement Object		
	   Statement stmt = con.createStatement();	
	   
	   System.out.println("abc");

			// Execute the SQL Query. Store results in ResultSet		
 		ResultSet rs= stmt.executeQuery(query);							
 
 		// While Loop to iterate through all data and print results		
		while (rs.next()){
	        		String myName = rs.getString(1);								        
                    String myAge = rs.getString(2);					                               
                    System. out.println(myName+"  "+myAge);		
            }		
			 // closing DB Connection		
			con.close();			
}


}
